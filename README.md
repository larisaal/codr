# CoDR - Code Data Visualizer & Recommender #

**Requirement:** Using the public data offered by the Free Code Camp, implement a Web service-oriented application able to explore, filter, visualize, and update the information regarding the participants to various programming competitions and their submitted solutions. The system will be able to provide useful recommendations about additional algorithm-related problems, contests, challenges, tutorials, public code repositories. In conjunction to DBpedia and/or Wikidata, the access to desired data and functionalities will be given via a SPARQL endpoint in various formats (at least HTML + RDFa and JSON-LD having embedded software source code schema.org constructs). See also, Stack Exchange Data Explorer.

**See our blog:** http://codr.weebly.com/

**Technical Report:** http://codr.weebly.com/technical-report.html

**User Guide:** http://codr.weebly.com/user-guide.html

**API Documentation:** http://codrapi.azurewebsites.net/Help

**Web App:** http://codrweb.azurewebsites.net/#/login